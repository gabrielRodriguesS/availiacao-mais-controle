import {User} from './user';

export class BudgetData {
  id?: number;
  propertyValue: number;
  interestRate: number;
  installmentNumber: number;
  user: User;
}
