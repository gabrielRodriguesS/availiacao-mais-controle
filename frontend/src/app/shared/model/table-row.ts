export class TableRow {
  openingBalance: number;
  interestRate: number;
  openingBalanceWithInterestRate: number;
  amortization: number;
  installmentValue: number;
  balanceUpdated: number;
}
