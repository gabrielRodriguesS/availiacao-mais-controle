import {Injectable} from '@angular/core';
import {AbstractService} from './abstract-service';
import {User} from '../model/user';
import {HttpClient} from '@angular/common/http';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService extends AbstractService<User> {

  private outputUserForm = new Subject<boolean>();
  observerOnOutputForm = this.outputUserForm.asObservable();

  constructor(http: HttpClient) {
    super(http, '/user');
  }

  sendEventToOutputForm(): void {
    this.outputUserForm.next();
  }
}
