import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/internal/Observable';
import {retry} from 'rxjs/operators';

export abstract class AbstractService<Entity> {
  url: string = environment.apiUrl;

  constructor(private http: HttpClient, private endPoint: string) {
    if (endPoint) {
      this.url = this.url + endPoint;
    }
  }

  public get(): Observable<Array<Entity>> {
    return this.http.get<Array<Entity>>(this.url).pipe(retry(3));
  }

  public getTo(path: string): Observable<Entity> {
    return this.http.get<Entity>(`${this.url}/${path}`);
  }

  public post(entity: Entity): Observable<Entity> {
    return this.http.post<Entity>(this.url, entity);
  }

}
