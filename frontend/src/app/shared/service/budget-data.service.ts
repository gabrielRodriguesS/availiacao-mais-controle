import {Injectable} from '@angular/core';
import {BudgetData} from '../model/budget-data';
import {HttpClient} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {environment} from '../../../environments/environment';
import {TableRow} from '../model/table-row';

@Injectable({
  providedIn: 'root'
})
export class BudgetDataService {

  url: string = environment.apiUrl;
  private outputBudgetDataForm = new Subject<boolean>();
  observerOnOutputForm = this.outputBudgetDataForm.asObservable();

  constructor(private http: HttpClient) {
  }

  sendEventToOutputForm(): void {
    this.outputBudgetDataForm.next();
  }

  calculateTable(budgetData: BudgetData, path: 'price' | 'sac'): Observable<TableRow[]> {
    return this.http.post<TableRow[]>(`${this.url}/budget-data/${path}`, budgetData);
  }

  searchBudgets(email: string, page = 0): Observable<any> {
    return this.http.get(`${this.url}/budget-data?email=${email}&size=3&page=${page}`);
  }
}
