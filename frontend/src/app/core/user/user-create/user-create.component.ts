import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from 'src/app/shared/service/user.service';
import {User} from '../../../shared/model/user';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent implements OnInit, OnDestroy {
  @Input() user: User;
  @Output() getUserData = new EventEmitter<User>();
  subscription: Subscription;
  userForm: FormGroup;

  validationsMessages = {
    nameError: 'Nome é um campo obrigatório.',
    emailError: 'E-mail é um campo obrigatório.'
  };

  constructor(
    private formBuilder: FormBuilder,
    private service: UserService
  ) {
  }

  ngOnInit() {
    this.createForm();
    this.subscribeObserver();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  createForm(): void {
    this.userForm = this.formBuilder.group({
      name: [this.user.name, [Validators.required]],
      email: [this.user.email, [Validators.email, Validators.required]]
    });
  }

  private markAsDirtyForm() {
    Object.keys(this.userForm.controls).forEach(key => {
      this.userForm.get(key).markAsDirty();
      this.userForm.get(key).updateValueAndValidity();
    });
  }

  private subscribeObserver() {
    this.subscription = this.service.observerOnOutputForm.subscribe(() => {
      this.markAsDirtyForm();
      if (this.userForm.valid) {
        this.getUserData.emit(this.userForm.value);
      } else {
        this.getUserData.emit(null);
      }
    });
  }
}
