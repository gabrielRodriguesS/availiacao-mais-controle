import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {BudgetSearchComponent} from './budget-search/budget-search.component';

const routes: Routes = [
  {
    path: '',
    component: BudgetSearchComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BudgetListRoutingModule {
}
