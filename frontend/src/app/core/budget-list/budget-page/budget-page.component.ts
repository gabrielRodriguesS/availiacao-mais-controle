import {Component, Input, OnInit} from '@angular/core';
import {BudgetDataService} from '../../../shared/service/budget-data.service';

@Component({
  selector: 'app-budget-page',
  templateUrl: './budget-page.component.html',
  styleUrls: ['./budget-page.component.css']
})
export class BudgetPageComponent implements OnInit {

  @Input() result: any;
  @Input() email: string;

  constructor(private service: BudgetDataService) {
  }

  ngOnInit() {
  }


  searchData() {
    this.result.number -= 1;
    this.service.searchBudgets(this.email, this.result.number).subscribe(value => {
      value.number += 1;
      this.result = value;
    });
  }
}
