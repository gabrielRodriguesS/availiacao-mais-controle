import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BudgetSearchComponent } from './budget-search.component';

describe('BudgetSearchComponent', () => {
  let component: BudgetSearchComponent;
  let fixture: ComponentFixture<BudgetSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BudgetSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BudgetSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
