import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BudgetDataService} from '../../../shared/service/budget-data.service';
import {NzNotificationService} from 'ng-zorro-antd';

@Component({
  selector: 'app-budget-search',
  templateUrl: './budget-search.component.html',
  styleUrls: ['./budget-search.component.css']
})
export class BudgetSearchComponent implements OnInit {

  userEmailForm: FormGroup;
  showResult = false;
  pageResult: any;
  userEmail: string;
  validationsMessages = {
    emailError: 'Use um endereço válido.'
  };
  notificationMessages = {
    emptySearch: 'Nenhum resultado foi encontrado'
  };

  constructor(private formBuilder: FormBuilder,
              private notification: NzNotificationService,
              private service: BudgetDataService) {
  }

  ngOnInit() {
    this.createForm();
  }

  private createForm() {
    this.userEmailForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    });
  }

  private markAsDirtyForm() {
    Object.keys(this.userEmailForm.controls).forEach(key => {
      this.userEmailForm.get(key).markAsDirty();
      this.userEmailForm.get(key).updateValueAndValidity();
    });
  }

  search() {
    this.markAsDirtyForm();
    if (this.userEmailForm.valid) {
      this.userEmail = this.userEmailForm.get('email').value;
      this.service.searchBudgets(this.userEmail).subscribe(value => {
        this.pageResult = value;
        this.showResult = true;
      }, () => {
        this.notification.info(this.notificationMessages.emptySearch, '');
      });
    }
  }
}
