import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {BudgetListRoutingModule} from './budget-list-routing.module';
import {BudgetSearchComponent} from './budget-search/budget-search.component';
import {BudgetPageComponent} from './budget-page/budget-page.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgZorroAntdModule} from 'ng-zorro-antd';
import {NgxMaskModule} from 'ngx-mask';

@NgModule({
  declarations: [BudgetSearchComponent, BudgetPageComponent],
  imports: [
    CommonModule,
    BudgetListRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgZorroAntdModule,
    NgxMaskModule.forRoot()
  ]
})
export class BudgetListModule {
}
