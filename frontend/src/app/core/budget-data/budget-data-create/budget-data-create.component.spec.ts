import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BudgetDataCreateComponent } from './budget-data-create.component';

describe('BudgetDataCreateComponent', () => {
  let component: BudgetDataCreateComponent;
  let fixture: ComponentFixture<BudgetDataCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BudgetDataCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BudgetDataCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
