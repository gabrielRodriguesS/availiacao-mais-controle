import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BudgetData} from '../../../shared/model/budget-data';
import {BudgetDataService} from '../../../shared/service/budget-data.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-budget-data-create',
  templateUrl: './budget-data-create.component.html',
  styleUrls: ['./budget-data-create.component.css']
})
export class BudgetDataCreateComponent implements OnInit, OnDestroy {
  @Input() budgetData: BudgetData;
  @Output() getBudgetData = new EventEmitter<BudgetData>();
  subscription: Subscription;
  budgetDataForm: FormGroup;

  validationsMessages = {
    propertyValueError: 'Preencha corretamente o valor do imóvel.',
    interestRateError: 'Preencha corretamente a taxa de juros.',
    installmentNumberError: 'Preencha corretamente o numero de parcelas.'
  };

  constructor(private formBuilder: FormBuilder, private service: BudgetDataService) {
  }

  ngOnInit() {
    this.createForm();
    this.subscribeObserver();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  createForm(): any {
    this.budgetDataForm = this.formBuilder.group({
      propertyValue: [this.budgetData.propertyValue, Validators.required],
      interestRate: [this.budgetData.interestRate, Validators.required],
      installmentNumber: [this.budgetData.installmentNumber, Validators.required]
    });
  }

  private markAsDirtyForm() {
    Object.keys(this.budgetDataForm.controls).forEach(key => {
      this.budgetDataForm.get(key).markAsDirty();
      this.budgetDataForm.get(key).updateValueAndValidity();
    });
  }

  private subscribeObserver() {
    this.subscription = this.service.observerOnOutputForm.subscribe(() => {
      this.markAsDirtyForm();
      if (this.budgetDataForm.valid) {
        this.getBudgetData.emit(this.budgetDataForm.value);
      } else {
        this.getBudgetData.emit(null);
      }
    });

  }

}
