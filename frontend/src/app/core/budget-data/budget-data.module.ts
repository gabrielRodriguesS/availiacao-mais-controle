import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';

import {BudgetDataRoutingModule} from './budget-data-routing.module';
import {BudgetDataCreateComponent} from './budget-data-create/budget-data-create.component';
import {NgZorroAntdModule} from 'ng-zorro-antd';
import {NgxMaskModule} from 'ngx-mask';

@NgModule({
  declarations: [BudgetDataCreateComponent],
  imports: [
    CommonModule,
    BudgetDataRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgZorroAntdModule,
    NgxMaskModule.forRoot()
  ],
  exports: [BudgetDataCreateComponent]
})
export class BudgetDataModule {
}
