import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BudgetDataCreateComponent } from './budget-data-create/budget-data-create.component';

const routes: Routes = [
  {
    path: '',
    component: BudgetDataCreateComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BudgetDataRoutingModule { }
