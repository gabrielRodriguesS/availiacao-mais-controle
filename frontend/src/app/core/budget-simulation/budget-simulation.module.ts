import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {BudgetSimulationRoutingModule} from './budget-simulation-routing.module';
import {SimulationComponent} from './simulation/simulation.component';
import {UserModule} from '../user/user.module';
import {BudgetDataModule} from '../budget-data/budget-data.module';
import {NgZorroAntdModule} from 'ng-zorro-antd';
import {SimulationResultComponent} from './simulation-result/simulation-result.component';

@NgModule({
  declarations: [SimulationComponent, SimulationResultComponent],
  imports: [
    CommonModule,
    BudgetSimulationRoutingModule,
    UserModule,
    BudgetDataModule,
    NgZorroAntdModule
  ],
  entryComponents: [SimulationResultComponent]
})
export class BudgetSimulationModule { }
