import {Component, Input, OnInit} from '@angular/core';
import {TableRow} from '../../../shared/model/table-row';

@Component({
  selector: 'app-simulation-result',
  templateUrl: './simulation-result.component.html',
  styleUrls: ['./simulation-result.component.css']
})
export class SimulationResultComponent implements OnInit {

  @Input() result: TableRow[];

  constructor() {
  }

  ngOnInit() {
  }

}
