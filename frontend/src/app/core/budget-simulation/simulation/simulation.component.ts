import {Component, OnInit} from '@angular/core';
import {BudgetData} from '../../../shared/model/budget-data';
import {BudgetDataService} from '../../../shared/service/budget-data.service';
import {UserService} from '../../../shared/service/user.service';
import {User} from '../../../shared/model/user';
import {NzModalService} from 'ng-zorro-antd';
import {SimulationResultComponent} from '../simulation-result/simulation-result.component';
import {TableRow} from '../../../shared/model/table-row';

@Component({
  selector: 'app-simulation',
  templateUrl: './simulation.component.html',
  styleUrls: ['./simulation.component.css']
})
export class SimulationComponent implements OnInit {
  budgetData: BudgetData = new BudgetData();
  user: User = new User();
  currentStep = 0;
  method: 'price' | 'sac' = 'price';

  constructor(private budgetDataService: BudgetDataService,
              private userService: UserService,
              private modalService: NzModalService) {
  }

  ngOnInit() {
  }

  validFormAndGoToNextStep(method: 'price' | 'sac') {
    this.method = method;
    this.budgetDataService.sendEventToOutputForm();
    this.userService.sendEventToOutputForm();
  }

  nextStep(): void {
    this.currentStep += 1;
  }

  preStep(): void {
    this.currentStep -= 1;
  }

  doSimulation() {
    this.userService.getTo(this.user.email).subscribe(value => {
      this.processResponse(value);
    }, () => {
      this.userService.post(this.user).subscribe(value => {
        this.processResponse(value);
      });
    });
  }

  getBudgetData(budgetData: BudgetData) {
    if (budgetData !== null) {
      this.budgetData = budgetData;
      console.log(this.budgetData);
      this.nextStep();
    }
  }

  getUserData(userData: User) {
    if (userData !== null) {
      this.user = userData;
      console.log(this.user);
      this.doSimulation();
    }
  }

  private showResult(result: TableRow[]) {
    const modal = this.modalService.create({
      nzTitle: 'Resultado da Simulação',
      nzContent: SimulationResultComponent,
      nzComponentParams: {
        result: result
      },
      nzWidth: 900,
      nzFooter: null
    });
  }

  private processResponse(value: User) {
    this.user = value;
    this.budgetData.user = this.user;
    this.budgetDataService.calculateTable(this.budgetData, this.method).subscribe(result => {
      this.showResult(result);
    });
  }
}
