import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: './core/budget-simulation/budget-simulation.module#BudgetSimulationModule'
  },
  {
    path: 'consultar',
    loadChildren: './core/budget-list/budget-list.module#BudgetListModule'
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
