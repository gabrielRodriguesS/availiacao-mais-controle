package com.avaliacao.maiscontrole.controller;

import com.avaliacao.maiscontrole.domain.model.BudgetData;
import com.avaliacao.maiscontrole.domain.model.PriceTableRow;
import com.avaliacao.maiscontrole.domain.model.SacTableRow;
import com.avaliacao.maiscontrole.service.BudgetDataService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("budget-data")
@RequiredArgsConstructor
public class BudgetDataController {
    private final BudgetDataService service;

    @PostMapping("/price")
    public List<PriceTableRow> calculatePriceTable(@RequestBody @Valid BudgetData data) {
        return this.service.calculatePriceTable(data);
    }

    @PostMapping("/sac")
    public List<SacTableRow> calculateSacTable(@RequestBody @Valid BudgetData data) {
        return this.service.calculateSacTable(data);
    }

    @GetMapping(params = "email")
    public ResponseEntity<?> getByEmail(@RequestParam("email") String email, Pageable pageRequest) {
        return this.service.getByEmail(email, pageRequest);
    }
}
