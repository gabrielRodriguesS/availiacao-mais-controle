package com.avaliacao.maiscontrole.domain.repository;

import com.avaliacao.maiscontrole.domain.model.BudgetData;
import com.avaliacao.maiscontrole.domain.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BudgetDataRepository extends JpaRepository<BudgetData, String> {
    Page<BudgetData> findByUser(User user, Pageable pageable);
}
