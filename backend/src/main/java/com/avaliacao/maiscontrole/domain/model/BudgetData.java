package com.avaliacao.maiscontrole.domain.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
public class BudgetData {
    @Id
    @GeneratedValue
    private Long id;
    @NotNull(message = "PropertyValue can not be null.")
    private Double propertyValue;
    @NotNull(message = "InterestRate can not be null.")
    private Double interestRate;
    @NotNull(message = "InstallmentNumber can not be null.")
    private Integer installmentNumber;
    @ManyToOne
    @JoinColumn
    @NotNull(message = "User can not be null.")
    private User user;
}
