package com.avaliacao.maiscontrole.domain.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PriceTableRow {
    private Double openingBalance;
    private Double interestRate;
    private Double openingBalanceWithInterestRate;
    private Double amortization;
    private Double installmentValue;
    private Double balanceUpdated;

    public Double getAmortization() {
        return this.installmentValue - this.interestRate;
    }

    public Double getOpeningBalanceWithInterestRate() {
        return this.openingBalance + this.interestRate;
    }

    public Double getBalanceUpdated() {
        Double balanceUpdated = this.getOpeningBalanceWithInterestRate() - this.installmentValue;
        return Double.valueOf(balanceUpdated.intValue());
    }
}
