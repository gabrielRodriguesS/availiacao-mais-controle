package com.avaliacao.maiscontrole.service;

import com.avaliacao.maiscontrole.domain.model.BudgetData;
import com.avaliacao.maiscontrole.domain.model.PriceTableRow;
import com.avaliacao.maiscontrole.domain.model.SacTableRow;
import com.avaliacao.maiscontrole.domain.model.User;
import com.avaliacao.maiscontrole.domain.repository.BudgetDataRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

@Service
@RequiredArgsConstructor
public class BudgetDataService {
    private final BudgetDataRepository repository;
    private final UserService userService;

    public ResponseEntity<?> getByEmail(String email, Pageable pageable) {
        AtomicReference<ResponseEntity> reference = new AtomicReference<>(new ResponseEntity(HttpStatus.NOT_FOUND));
        Optional<User> optionalUser = this.userService.findByEmail(email);
        optionalUser.ifPresent(user -> {
            Page<BudgetData> budgetDataPage = this.repository.findByUser(user, pageable);
            reference.set(new ResponseEntity(budgetDataPage, HttpStatus.OK));
        });
        return reference.get();
    }

    public List<PriceTableRow> calculatePriceTable(BudgetData data) {
        List<PriceTableRow> priceTableRows = new ArrayList<>();
        PriceTableRow tableRow;
        Double installmentValue = this.calculateInstallmentValue(data);
        this.saveBudgetData(data);
        int i = 0;
        while (i < data.getInstallmentNumber()) {
            tableRow = PriceTableRow
                    .builder()
                    .openingBalance(data.getPropertyValue())
                    .interestRate(this.calculateInterestRate(data.getPropertyValue(), data.getInterestRate()))
                    .installmentValue(installmentValue)
                    .build();
            data.setPropertyValue(tableRow.getBalanceUpdated());
            priceTableRows.add(tableRow);
            i++;
        }
        return priceTableRows;
    }

    public List<SacTableRow> calculateSacTable(BudgetData data) {
        List<SacTableRow> sacTableRows = new ArrayList<>();
        SacTableRow tableRow;
        Double amortizationValue = this.calculatePriceAmortizationValue(data);
        this.saveBudgetData(data);
        int i = 0;
        while (i < data.getInstallmentNumber()) {
            tableRow = SacTableRow
                    .builder()
                    .openingBalance(data.getPropertyValue())
                    .amortization(amortizationValue)
                    .interestRate(this.calculateInterestRate(data.getPropertyValue(), data.getInterestRate()))
                    .build();
            tableRow.setInstallmentValue(this.calculatePriceInstallmentValue(tableRow.getInterestRate(), amortizationValue));
            data.setPropertyValue(tableRow.getBalanceUpdated());
            sacTableRows.add(tableRow);
            i++;
        }
        return sacTableRows;
    }

    private Double calculatePriceInstallmentValue(Double interestRate, Double amortizationValue) {
        return amortizationValue + interestRate;
    }

    private Double calculatePriceAmortizationValue(BudgetData data) {
        return data.getPropertyValue() / data.getInstallmentNumber();
    }

    @Async
    public void saveBudgetData(BudgetData data) {
        this.repository.save(data);
    }

    private Double calculateInstallmentValue(BudgetData data) {
        Double sumOneToInterestRate = (1 + data.getInterestRate());
        Double subtractOneToRatePow = (1 - Math.pow(sumOneToInterestRate, -data.getInstallmentNumber()));
        Double dividePeerInterestRate = subtractOneToRatePow / data.getInterestRate();
        return data.getPropertyValue() / dividePeerInterestRate;
    }

    private Double calculateInterestRate(Double propertyValue, Double interestRate) {
        return propertyValue * interestRate;
    }
}
