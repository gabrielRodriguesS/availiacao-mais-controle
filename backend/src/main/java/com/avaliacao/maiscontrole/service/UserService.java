package com.avaliacao.maiscontrole.service;

import com.avaliacao.maiscontrole.domain.model.User;
import com.avaliacao.maiscontrole.domain.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository repository;

    public User saveUser(User user) {
        return this.repository.save(user);
    }

    public Optional<User> findByEmail(String email) {
        return this.repository.findByEmail(email);
    }

    public ResponseEntity<?> getByEmail(String email) {
        AtomicReference<ResponseEntity> reference = new AtomicReference<>(new ResponseEntity(HttpStatus.NOT_FOUND));
        Optional<User> optionalUser = this.repository.findByEmail(email);
        optionalUser.ifPresent(user -> reference.set(new ResponseEntity(user, HttpStatus.OK)));
        return reference.get();
    }
}
