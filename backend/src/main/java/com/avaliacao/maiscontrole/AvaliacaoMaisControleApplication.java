package com.avaliacao.maiscontrole;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AvaliacaoMaisControleApplication {

    public static void main(String[] args) {
        SpringApplication.run(AvaliacaoMaisControleApplication.class, args);
    }

}
