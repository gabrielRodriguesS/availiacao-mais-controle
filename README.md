
# availiacao-mais-controle

## Como rodar o backend

- Tenha instalado
    -   Java
    -   Mysql
- Vá ate a pasta backend-para-deploy
- Execute o jar avaliacao-mais-controle-0.0.1-SNAPSHOT.jar com o comando:
    -  java -jar ./avaliacao-mais-controle-0.0.1-SNAPSHOT.jar --spring.datasource.username=NOME-DO-SEU-USUARIO --spring.datasource.password=SUA-SENHA-DO-BANCO
- Pronto o backend deverá subir normalmente (espero eu!).

## Como rodar o frontend


Você tem duas opções

- 1º Opção 
	- Pegue o projeto compilado na pasta frontend-para-deploy  
	- Configure seu servidor web (apache ou nginx)
	- E coloque ele rodando no seu servidor web
- 2º Opção
	- Tenha o angular cli instalado ([Tutorial para instalar](https://www.npmjs.com/package/@angular/cli))
	- Vá ate a pasta frontend 
	- Execute o comando ng serve
   
